Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: PDFminer
Upstream-Contact: Yusuke Shinyama <yusuke@cs.nyu.edu>
Source: https://github.com/pdfminer/pdfminer.six/tags
Files-Excluded: samples/nonfree/*
Comment:
 All but trivial test PDF documents were removed from the sources/ directory
 because of lack of source code for them.

Files: *
Copyright: 2004-2016, Yusuke Shinyama <yusuke@cs.nyu.edu>
License: Expat

Files: cmaprsrc/*.txt
Copyright: 1990-2010, Adobe Systems Incorporated
License: BSD-Adobe
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 .
 Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 Neither the name of Adobe Systems Incorporated nor the names of its
 contributors may be used to endorse or promote products derived from this
 software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

Files: pdfminer/glyphlist.py
Copyright: 1997-2007, Adobe Systems Incorporated
License: other-1
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this documentation file to use, copy, publish, distribute, sublicense,
 and/or sell copies of the documentation, and to permit others to do the same,
 provided that:
 .
 - No modification, editing or other alteration of this document is allowed;
 and
 .
 - The above copyright notice and this permission notice shall be included in
 all copies of the documentation.
 .
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this documentation file, to create their own derivative works from the
 content of this document to use, copy, publish, distribute, sublicense,
 and/or sell the derivative works, and to permit others to do the same,
 provided that the derived work is not represented as being a copy or version
 of this document.
 .
 Adobe shall not be liable to any party for any loss of revenue or profit or
 for indirect, incidental, special, consequential, or other similar damages,
 whether based on tort (including without limitation negligence or strict
 liability), contract or other legal or equitable grounds even if Adobe has
 been advised or had reason to know of the possibility of such damages. The
 Adobe materials are provided on an "AS IS" basis. Adobe specifically
 disclaims all express, statutory, or implied warranties relating to the Adobe
 materials, including but not limited to those concerning merchantability or
 fitness for a particular purpose or non-infringement of any third party
 rights regarding the Adobe materials.

Files: pdfminer/fontmetrics.py
Copyright: 1985-1999, Adobe Systems Incorporated.
License: other-2
 This file and the 35 PostScript® AFM files it accompanies may be used,
 copied, and distributed for any purpose and without charge, with or without
 modification, provided that all copyright notices are retained; that the AFM
 files are not distributed without this file; that all modifications to this
 file or any of the AFM files are prominently noted in the modified file(s);
 and that this paragraph is not modified. Adobe Systems has no responsibility
 or obligation to support the use of the AFM files.

Files: pdfminer/runlength.py
Copyright: Public Domain
License: public-domain
 This code is in the public domain.
 .
 rijndael.py is based on a public domain C implementation
 by Philip J. Erdelsky: http://www.efgh.com/software/rijndael.htm
 .
 runlength.py: RunLength decoder (Adobe version) implementation based
 on PDF Reference version 1.4 section 3.3.4.

Files: samples/jo.*
Copyright: expired
Comment: Kenji Miyazawa, preface of "Haru to Shura"
License: public-domain
 This files are in the public domain because copyright expired: Kenji Miyazawa
 died on 21 September 1933.

Files: debian/*
Copyright: 2010, Jakub Wilk <jwilk@debian.org>
           2011-2022 Daniele Tricoli <eriol@debian.org>
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
