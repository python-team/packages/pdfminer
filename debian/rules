#!/usr/bin/make -f

include /usr/share/dpkg/pkg-info.mk

export PYBUILD_NAME=pdfminer
export PYTHONWARNINGS=d
export PYBUILD_TEST_ARGS=pdfminer/*.py tests/test_pdfminer_*.py

debroot = debian/tmp/
py3_libdir = usr/lib/python3/dist-packages

# Upstream uses a CI job to write version before publishing the package to
# PyPI and upstream changelog, unfortunately, doesn't list every release so we
# resolve the version from the debian/changelog.
VERSION = $(shell echo $(DEB_VERSION) | cut -d+ -f1)

%:
	dh $@ --with python3 --buildsystem=pybuild

execute_before_dh_auto_configure execute_before_dh_auto_clean:
	sed -i "s/__VERSION__/${VERSION}/g" pdfminer/__init__.py

override_dh_auto_build:
	$(MAKE) cmap
	dh_auto_build

override_dh_auto_install:
	pybuild --install -p 3

	# Move cmap files outside dist-packages
	mkdir -p $(debroot)/usr/share/pdfminer/
	mv debian/python3-pdfminer/$(py3_libdir)/pdfminer/cmap/*.pickle.gz \
		$(debroot)/usr/share/pdfminer/

	rename 's/\.py$$//' debian/python3-pdfminer/usr/bin/*.py

override_dh_installman:
	$(MAKE) -C debian/manpages/
	dh_installman

# vim:ts=4 sw=4 noet
